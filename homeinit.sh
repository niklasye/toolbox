#!/bin/bash

set -e

# Copy files
cp --recursive --backup --no-target-directory /opt/homeinit ${HOME}
printf "\xE2\x9C\x94 Copy files to home directory\n"

# Setup git dir
if [ -d "${HOME}/git" ] 
then
	printf "\xE2\x9C\x94 Git directory exists\n"
else
	mkdir ${HOME}/git
	printf "\xE2\x9C\x94 Create Git directory\n"
fi

# Setup zsh syntax highlighting
if [ -d "${HOME}/git/zsh-syntax-highlighting" ] 
then
	printf "\xE2\x9C\x94 zsh syntax highlighting already installed\n"
else
	git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${HOME}/git/zsh-syntax-highlighting
	printf "\xE2\x9C\x94 Clone zsh syntax highlighting\n"
fi

# Setup vimplug
curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
vim +'PlugInstall --sync' +qa
printf "\xE2\x9C\x94 VimPlug installed\n"
