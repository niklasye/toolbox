FROM registry.fedoraproject.org/f32/fedora-toolbox:32

# Setup repositories
RUN sudo dnf install -y https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm

# Install software
RUN sudo dnf install -y \
	vim \
	tmux \
	ranger \
	git \
	rsync \
	zsh \
	newsboat

# Copy configurations
COPY homeinit /opt/homeinit
COPY homeinit.sh /usr/local/bin/homeinit.sh
RUN chmod 0755 /usr/local/bin/homeinit.sh
RUN chmod -R o+r /opt/homeinit
