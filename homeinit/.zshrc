# =============
#    INIT
# =============
# Senstive functions which are not pushed to Github
# It contains GOPATH, some functions, aliases etc...
[ -r ~/.zsh_private ] && source ~/.zsh_private

# Add to fpath
export fpath=(~/.zsh/completion $fpath)

# Add to path
export PATH=~/bin:~/development/tools/apache-maven-3.3.9/bin:$PATH

# =============
#    ALIAS
# =============
alias ..='cd ..'
alias ls='ls -GpF' # Mac OSX specific
alias ll='ls -alGpF' # Mac OSX specific

alias vi='vim'

alias homegit="GIT_DIR=~/git/homegit/.git GIT_WORK_TREE=~ git"

alias k="kubectl"
alias kn="kubectl config set-context --current --namespace"

alias wiki="vim ~/git/enokiwiki/docker/site/content/docs"
alias diary="vim ~/git/notes/diary/$(date '+%Y-%m-%d').md"
alias yesterday="vim ~/git/notes/diary/$(date '+%Y-%m-%d' --date '1 day ago').md"
alias todo="vim ~/git/notes/todo.md"

# =============
#    EXPORT
# =============
export EDITOR="vim"
export LSCOLORS=cxBxhxDxfxhxhxhxhxcxcx
export CLICOLOR=1

# support colors in less
export LESS_TERMCAP_mb=$'\E[01;31m'
export LESS_TERMCAP_md=$'\E[01;31m'
export LESS_TERMCAP_me=$'\E[0m'
export LESS_TERMCAP_se=$'\E[0m'
export LESS_TERMCAP_so=$'\E[01;44;33m'
export LESS_TERMCAP_ue=$'\E[0m'
export LESS_TERMCAP_us=$'\E[01;32m'

# =============
#    HISTORY
# =============

## Command history configuration
if [ -z "$HISTFILE" ]; then
    HISTFILE=$HOME/.zsh_history
fi

HISTSIZE=1000000
SAVEHIST=1000000

# Add timestamp and execution time to history
setopt EXTENDED_HISTORY

# Remove duplicated entries first
setopt HIST_EXPIRE_DUPS_FIRST

# Ignore duplication command history list
setopt HIST_IGNORE_DUPS

# Ignore commands that starts with space
setopt HIST_IGNORE_SPACE

# Don't run command when selected in history, just paste it on commandline
setopt HIST_VERIFY

# Write history as each command is exectured
setopt INC_APPEND_HISTORY

# Share command history data
setopt SHARE_HISTORY

# Don't show duplicates in history search
setopt HIST_FIND_NO_DUPS

# Sync the note repository
function notesync() {
	git --work-tree="/home/${USER}/git/notes" --git-dir="/home/${USER}/git/notes/.git" pull origin master
	git --work-tree="/home/${USER}/git/notes" --git-dir="/home/${USER}/git/notes/.git" add .
	git --work-tree="/home/${USER}/git/notes" --git-dir="/home/${USER}/git/notes/.git" commit -m 'Commit by notesync'
	git --work-tree="/home/${USER}/git/notes" --git-dir="/home/${USER}/git/notes/.git" push origin master
}

# Sync the wiki repository
function wikisync() {
	git --work-tree="/home/${USER}/git/enokiwiki" --git-dir="/home/${USER}/git/enokiwiki/.git" pull origin master
	git --work-tree="/home/${USER}/git/enokiwiki" --git-dir="/home/${USER}/git/enokiwiki/.git" add .
	git --work-tree="/home/${USER}/git/enokiwiki" --git-dir="/home/${USER}/git/enokiwiki/.git" commit -m 'Commit by wikisync'
	git --work-tree="/home/${USER}/git/enokiwiki" --git-dir="/home/${USER}/git/enokiwiki/.git" push origin master
}

function rpull() {
	if [[ ! -a .rsync ]]; then
		echo 'Directory not configured for sync'
	else
		source .rsync
		rsync -aP $RSYNC_REMOTE .
	fi
}

function rpush() {
	if [[ ! -a .rsync ]]; then
		echo 'Directory not configured for sync'
	else
		source .rsync
		rsync -aP . $RSYNC_REMOTE
	fi
}

# =============
#    PROMPT
# =============
autoload -U colors && colors
setopt promptsubst

kube_off=
function kt() { [[ -v kube_off ]] && unset kube_off || kube_off= }

local ret_status="%(?:%{$fg_bold[green]%}$:%{$fg_bold[green]%}$)"
PROMPT='${ret_status} %{$fg[cyan]%}%c%{$reset_color%} $(git_prompt_info)%{$fg_bold[blue]%}${kube_off-$ZSH_THEME_K8S_PROMPT_PREFIX$(k8s_context)/$(k8s_namespace)$ZSH_THEME_K8S_PROMPT_SUFFIX}%{$reset_color%} '

ZSH_THEME_K8S_PROMPT_PREFIX="%{$fg_bold[blue]%}k8s:(%{$fg_bold[red]%}"
ZSH_THEME_K8S_PROMPT_SUFFIX="%{$fg_bold[blue]%})"
ZSH_THEME_GIT_PROMPT_PREFIX="%{$fg_bold[blue]%}git:(%{$fg[red]%}"
ZSH_THEME_GIT_PROMPT_SUFFIX="%{$reset_color%} "
ZSH_THEME_GIT_PROMPT_DIRTY="%{$fg[blue]%}) %{$fg[yellow]%}✗"
ZSH_THEME_GIT_PROMPT_CLEAN="%{$fg[blue]%})"

# k8s
function k8s_context() {
	kubectl config current-context
}

function k8s_namespace() {
	kubectl config view --minify --output 'jsonpath={..namespace}'
}

# Outputs current branch info in prompt format
function git_prompt_info() {
  local ref
  if [[ "$(command git config --get customzsh.hide-status 2>/dev/null)" != "1" ]]; then
    ref=$(command git symbolic-ref HEAD 2> /dev/null) || \
    ref=$(command git rev-parse --short HEAD 2> /dev/null) || return 0
    echo "$ZSH_THEME_GIT_PROMPT_PREFIX${ref#refs/heads/}$(parse_git_dirty)$ZSH_THEME_GIT_PROMPT_SUFFIX"
  fi
}

# Checks if working tree is dirty
function parse_git_dirty() {
  local STATUS=''
  local FLAGS
  FLAGS=('--porcelain')

  if [[ "$(command git config --get customzsh.hide-dirty)" != "1" ]]; then
    FLAGS+='--ignore-submodules=dirty'
    STATUS=$(command git status ${FLAGS} 2> /dev/null | tail -n1)
  fi

  if [[ -n $STATUS ]]; then
    echo "$ZSH_THEME_GIT_PROMPT_DIRTY"
  else
    echo "$ZSH_THEME_GIT_PROMPT_CLEAN"
  fi
}

# ===================
#    AUTOCOMPLETION
# ===================
# enable completion
autoload -Uz compinit

# Run compinit if the dump is older than 24 hours
if [[ -n ${ZDOTDIR}/.zcompdump(#qN.mh+24) ]]; then
	compinit;
else
	compinit -C;
fi;

zmodload -i zsh/complist

WORDCHARS=''

unsetopt menu_complete   # do not autoselect the first completion entry
unsetopt flowcontrol
setopt auto_menu         # show completion menu on successive tab press
setopt complete_in_word
setopt always_to_end

# autocompletion with an arrow-key driven interface
zstyle ':completion:*:*:*:*:*' menu select

zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}' 'r:|=*' 'l:|=* r:|=*'
zstyle ':completion:*' list-colors ''
zstyle ':completion:*:*:kill:*:processes' list-colors '=(#b) #([0-9]#) ([0-9a-z-]#)*=01;34=0=01'

zstyle ':completion:*:*:*:*:processes' command "ps -u $USER -o pid,user,comm -w -w"

# Don't complete uninteresting users
zstyle ':completion:*:*:*:users' ignored-patterns \
        adm amanda apache at avahi avahi-autoipd beaglidx bin cacti canna \
        clamav daemon dbus distcache dnsmasq dovecot fax ftp games gdm \
        gkrellmd gopher hacluster haldaemon halt hsqldb ident junkbust kdm \
        ldap lp mail mailman mailnull man messagebus  mldonkey mysql nagios \
        named netdump news nfsnobody nobody nscd ntp nut nx obsrun openvpn \
        operator pcap polkitd postfix postgres privoxy pulse pvm quagga radvd \
        rpc rpcuser rpm rtkit scard shutdown squid sshd statd svn sync tftp \
        usbmux uucp vcsa wwwrun xfs '_*'

zstyle '*' single-ignored show

# Automatically update PATH entries
zstyle ':completion:*' rehash true

# Keep directories and files separated
zstyle ':completion:*' list-dirs-first true

# ===================
#    KEY BINDINGS
# ===================
# Use emacs-like key bindings by default:
bindkey -e

# [Ctrl-r] - Search backward incrementally for a specified string. The string
# may begin with ^ to anchor the search to the beginning of the line.
bindkey '^r' history-incremental-search-backward      

if [[ "${terminfo[kpp]}" != "" ]]; then
  bindkey "${terminfo[kpp]}" up-line-or-history       # [PageUp] - Up a line of history
fi

if [[ "${terminfo[knp]}" != "" ]]; then
  bindkey "${terminfo[knp]}" down-line-or-history     # [PageDown] - Down a line of history
fi

if [[ "${terminfo[khome]}" != "" ]]; then
  bindkey "${terminfo[khome]}" beginning-of-line      # [Home] - Go to beginning of line
fi

if [[ "${terminfo[kend]}" != "" ]]; then
  bindkey "${terminfo[kend]}"  end-of-line            # [End] - Go to end of line
fi
if [[ "${terminfo[kcbt]}" != "" ]]; then
  bindkey "${terminfo[kcbt]}" reverse-menu-complete   # [Shift-Tab] - move through the completion menu backwards
fi

bindkey '^?' backward-delete-char                     # [Backspace] - delete backward
if [[ "${terminfo[kdch1]}" != "" ]]; then
  bindkey "${terminfo[kdch1]}" delete-char            # [Delete] - delete forward
else
  bindkey "^[[3~" delete-char
  bindkey "^[3;5~" delete-char
  bindkey "\e[3~" delete-char
fi

# ===================
#    MISC SETTINGS
# ===================

# Open commandline in editor with ctrl-x ctrl-e
autoload -U edit-command-line
zle -N edit-command-line
bindkey '^xe' edit-command-line
bindkey '^x^e' edit-command-line

# Show dots while loading tab completion
expand-or-complete-with-dots() {
  echo -n "\e[31m...\e[0m"
  zle expand-or-complete
  zle redisplay
}
zle -N expand-or-complete-with-dots
bindkey "^I" expand-or-complete-with-dots

# Automatically remove duplicates from these arrays
typeset -U path PATH cdpath CDPATH fpath FPATH manpath MANPATH

# Load syntax highlighting
source ~/git/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

# Load plugins/completions
source ~/.zsh/plugins/*
source <(kubectl completion zsh)
