" Plugins, run PlugInstall after adding a new plugin
call plug#begin('~/.vim/plugged')

Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'w0rp/ale'
Plug 'morhetz/gruvbox'
Plug 'altercation/vim-colors-solarized'

call plug#end()

" Switch between solarized light and dark with F6
set background=dark " dark | light "                       
colorscheme gruvbox
call togglebg#map("<F6>")

syntax on
set number
set cursorline
set cursorcolumn
set relativenumber
set noautoindent
set nocompatible
filetype plugin on
filetype indent off

" Set color for matching braces/parethesis/brackets
hi MatchParen cterm=none ctermbg=green ctermfg=red

let ale_yaml_yamllint_options = "-c ~/.yamllint"

let g:airline_powerline_fonts = 0

" Enables permanent status line from vim-airline
set laststatus=2

" Set the max number of tabs
set tabpagemax=100

" Toggle line numbers
nnoremap <F2> :set nonumber! norelativenumber!<CR>

" Toggle ALE on and off
nnoremap <F3> :ALEToggle<CR>

let vimwiki_list = [{'path': '~/vimwiki/', 'syntax': 'markdown', 'ext': '.md'}]
